## Installation

1. Go to Downloads in the left panel
2. Download the p

## Usage

1. Go to terminal
2. Run the command: node index.js
3. Go to any internet explore, type the URL "localhost:xxxx"

## Requirements

1. Visual Studio Code
2. Node.js
3. Express farmwork 
4. MongoDB 

## Authors and acknowledgment

1. Guanlin Li as the authors
2. Diogo Lima as the author
3. Davneet Singh Chawla as the mentor

## License
```
MIT License

Copyright (c) [2020] [Guanlin Li]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

## About Wiki page

I create a new Wiki page instead of modifying the Home Wiki page