/*********************************GENERAL SETTINGS*********************************/
var path = require('path');

/******* Express Settings *******/
const express = require('express');
var dgcms = express();
dgcms.set('views', path.join(__dirname, 'views'));
dgcms.set('view engine', 'ejs');
dgcms.use(express.static(__dirname + '/public'));

/****** Bodyparser Settings ******/
const bodyparser = require('body-parser');
dgcms.use(bodyparser.urlencoded({ extended: false}));
dgcms.use(bodyparser.json());

/****** MongoDB Settings ******/
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/dgcms', {
    useNewUrlParser: true
});

/***** Express Validator *******/
const {check, validationResult} = require('express-validator');

/**** FileUpload *******/
const fileUpload = require('express-fileupload');
dgcms.use(fileUpload());

/**** Enabling Session *******/
const session = require('express-session');
dgcms.use(session({
    secret: 'superrandomsecret',
    resave: false,
    saveUninitialized: true
}));

/**** File System *******/
const fs = require('fs');

/***** Listening Port *******/
dgcms.listen(8080);
console.log('DGCMS Running on port 8080...');

/*********************************MODELS*********************************/
/****** Article ******/
var Article = mongoose.model ('Article', {
    Title: String,
    SectionId: String,
    Content: String,
    DateAdded: String,
    DateModified: String,
    User: String,
    Status: String,
    ImageName: String
});

/****** Section ******/
var Section = mongoose.model ('Section', {
    Section: String
});

/****** User ******/
var User = mongoose.model ('User', {
    UserName: String,
    Password: String,
    Name: String
});

/****** Config ******/
var Config = mongoose.model ('Config', {
    SystemName: String
});


/*********************************CONSTANTS*********************************/

var initialSections = [
    "Sports",
    "Games",
    "Music",
    "Movies",
    "Economy",
    "Politics",
    'About Us'
];

var initialUser = new User({
    UserName: 'admin',
    Password: 'admin',
    Name: 'System Administrator'
});

var initialConfig = new Config({
    SystemName: 'DGCMS'
});

var initialArticle = new Article({
    Title: "About Us",
    Content: "<h2>Hello!</h2><p>&nbsp;</p><p>This website was developed by Diogo Franco Ferraz de Lima (student number: 8652750) and Guanlin Li (student number: 8XXXXXX), for the project assignment of the course PROG8020: Programming: Web Design and Development taught by Prof. Davneet Singh Chawla, on Computer Applications Development program from Conestoga College in the Fall Semester of 2019.</p><p>&nbsp;</p><p>&nbsp;</p>",
    User: "admin",
    DateAdded: Date().toLocaleString(),
    DateModified: Date().toLocaleString(),
    Status: 'POSTED',
    ImageName: 'conestoga.png'
});

var isInitialized = false;

/*******************************INITIAL SETUP****************************/
Section.find({Section: 'About Us'}).exec(function (err, sections) {
    if (sections.length == 0){
        var id = '';
        initialSections.forEach(section => {   
            var newSection = new Section({
                Section: section
            });
            newSection.save();
            if (section == 'About Us')
                id = newSection.id.toString();
        });
        Object.assign(initialArticle, {SectionId: id});
        initialArticle.save();
        initialUser.save();
        initialConfig.save();
    }
});

/*********************************ROUTES*********************************/
/****** Index ******/
dgcms.get('/', function(req, res){
    Article.find({Status: 'POSTED'}).exec(function (err, articles) {
        //res.render('index', {articles: articles, sections: findSection});
        render(req, res, 'index', {articles: articles});
    });

});

/****** Zoom on Article  ******/
dgcms.get('/zoom/:id', function(req, res){
    var id = req.params.id;
    Article.find({_id: id, Status: 'POSTED'}).exec(function (err, articles) {
        render(req, res, 'zoom', {articles: articles});
    });
});

/****** LOGIN - GET *****/
dgcms.get('/login',function(req, res){
    render(req, res, 'login', {});
});

/****** LOGIN - POST *****/
dgcms.post('/login',function(req, res){
    var username = req.body.username;
    var password = req.body.password;

    User.findOne({UserName: username, Password: password }).exec(function (err, user) {
        if (user != null)
        {
            req.session.username = user.UserName;
            req.session.name = user.Name;
            req.session.userLoggedIn = true;
            res.redirect('/Admin');
        }
        else
        {
            var message = {
                Message: "Invalid user or password!"
            };
            render(req, res, 'login', message);
        }
    });
});

/****** Admin - Get ******/
dgcms.get('/Admin', function(req, res){
    if (req.session.userLoggedIn)
    {
        Article.find({}).exec(function (err, articles) {
            render(req, res, 'adminHome', {articles: articles});
        });
    }
    else
    {
        res.redirect('/login');
    }
});


/****** newArticle - Get ******/
dgcms.get('/newArticle', function(req, res){
    if (req.session.userLoggedIn)
    {
        render(req, res, 'newArticle', {status: 'NEW'});
    }
    else
    {
        res.redirect('/login');
    }
});

/****** newArticle - POST*****/
dgcms.post('/newArticle', [
    check('title', 'Please Enter the Title').not().isEmpty(),
    check('sectionId', 'Please Enter the Section').not().isEmpty(),
    check('content', 'Please Enter the Content').not().isEmpty(),
    check('image').custom((value, {req}) => {
        if (req.files == null){
            throw new Error('Please upload a Image');
        }
        return true;
    })
], function(req, res){
    if (req.session.userLoggedIn)
    {
        const errors = validationResult(req);
        var title = req.body.title;
        var sectionId = req.body.sectionId;
        var content = req.body.content;
        if (!errors.isEmpty()){
            var errorData = {
                errors: errors.array(),
                title: title,
                sectionId: sectionId,
                content: content,
                status: 'NEW'
            }
            render(req, res, 'newArticle', errorData);
        }
        else {
            //var user = session.userName;
            var imageName = generateImageName(req.files.image.name);
            var image = req.files.image;
            var imagepath = 'public/articleImages/' + imageName;
            image.mv(imagepath, function (err){
                console.log(err);
            });

            var newArticle = new Article({
                Title: title,
                SectionId: sectionId,
                Content: content,   
                User: req.session.username,
                DateAdded: Date().toLocaleString(),
                DateModified: Date().toLocaleString(),
                Status: 'DRAFT',
                ImageName: imageName
            });
    
            newArticle.save();
            res.redirect('/Admin');
        }
    }
    else
    {
        res.redirect('/login');
    }
});

/****** editArticle - Edit (GET) ******/
dgcms.get('/editArticle/:id', function(req, res){
    if (req.session.userLoggedIn)
    {
        var id = req.params.id;
        Article.findOne({_id: id}).exec(function (err, article){
            
            var currentArticle = {
                title: article.Title,
                content: article.Content,
                sectionId: article.SectionId,
                id: article._id,
                status: article.Status
            }
            
            render(req, res, 'newArticle', currentArticle);
        });
    }
    else
    {
        res.redirect('/login');
    }
});

/****** editArticle - Edit (POST) ******/
dgcms.post('/editArticle/:id', [
    check('title', 'Please Enter the Title').not().isEmpty(),
    check('sectionId', 'Please Enter the Section').not().isEmpty(),
    check('content', 'Please Enter the Content').not().isEmpty(),
    check('image').custom((value, {req}) => {
        if (req.files == null){
            throw new Error('Please upload a Image');
        }
        return true;
    })
], function(req, res){
    if (req.session.userLoggedIn)
    {
        const errors = validationResult(req);
        var title = req.body.title;
        var sectionId = req.body.sectionId;
        var content = req.body.content;
        var id = req.body.id;
        var status = req.body.status;

        if (!errors.isEmpty()){
            var errorData = {
                errors: errors.array(),
                title: title,
                content: content,
                sectionId: sectionId,
                id: id,
                status: status
            }
            render(req, res, 'newArticle', errorData);
        }
        else {

            var imageName = generateImageName(req.files.image.name);
            var image = req.files.image;
            var imagepath = 'public/articleImages/' + imageName;
            image.mv(imagepath, function (err){
                console.log(err);
            });

            var title = req.body.title;
            var sectionId = req.body.sectionId;
            var content = req.body.content;
            var id = req.params.id;
            var updateArticle = {
                Title: title,
                SectionId: sectionId,
                Content: content,   
                User: req.session.username,
                DateModified: Date().toLocaleString(),
                ImageName: imageName
            };
            //var user = session.userName;
            Article.findOneAndUpdate({_id: id}, updateArticle).exec(function () {
                res.redirect('/Admin');
            });
        }
    }
    else
    {
        res.redirect('/login');
    }
});

/****** AdminNew - Delete ******/
dgcms.get('/deleteArticle/:id', function(req, res){
    if (req.session.userLoggedIn)
    {
        var id = req.params.id;
        Article.findByIdAndDelete({_id: id}).exec(function (err){
            res.redirect('/Admin');
        });
    }
    else
    {
        res.redirect('/login');
    }
});

/****** AdminNew - POST ******/
dgcms.get('/postArticle/:id', function(req, res){
    if (req.session.userLoggedIn)
    {
        var id = req.params.id;
        Article.findOneAndUpdate({_id: id}, {
            Status: 'POSTED'
        }).exec(function (err){
            res.redirect('/Admin');
        });
    }
    else
    {
        res.redirect('/login');
    }
});

/****** AdminNew - UNPOST ******/
dgcms.get('/unpostArticle/:id', function(req, res){
    if (req.session.userLoggedIn)
    {
        var id = req.params.id;
        Article.findOneAndUpdate({_id: id}, {
            Status: 'DRAFT'
        }).exec(function (err){
            res.redirect('/Admin');
        });
    }
    else
    {
        res.redirect('/login');
    }
});


/****** changeName - Edit (GET) ******/
dgcms.get('/changeName', function(req, res){
    if (req.session.userLoggedIn)
    {
        render(req, res, 'systemName', {});
    }
    else
    {
        res.redirect('/login');
    }
});

/****** changeName - Edit (POST) ******/
dgcms.post('/changeName', [
    check('systemName', 'Please Enter the system name').not().isEmpty()
], function(req, res){
    if (req.session.userLoggedIn)
    {
        const errors = validationResult(req);
        var systemName = req.body.systemName.substring(0,10);

        if (!errors.isEmpty()){
            var errorData = {
                errors: errors.array(),
                SystemName: systemName
            }
            render(req, res, 'systemName', errorData);
        }
        else {
            var systemName = req.body.systemName;
            var updateConfig = {
                SystemName: systemName
            };
            //var user = session.userName;
            Config.findOneAndUpdate({}, updateConfig).exec(function () {
                res.redirect('/Admin');
            });
        }
    }
    else
    {
        res.redirect('/login');
    }
});

/****** LOGOUT *****/
dgcms.get('/logout',function(req, res){
    req.session.destroy();
    res.redirect('/');
});

/****** Index with filter ******/
dgcms.get('/:sectionId', function(req, res){
    var sectionId = req.params.sectionId;
    Article.find({SectionId: sectionId, Status: 'POSTED'}).exec(function (err, articles) {
        render(req, res, 'index', {articles: articles});
    });
});
/*********************************FUNCTIONS*********************************/

function render(reqObj, resObj, view, data) {    
    Section.find({}).exec(function (err, sections){
        sections = {sections: sections};
        Object.assign(sections, data);
        if (typeof reqObj.session.userLoggedIn !== 'undefined')
        {
            var loginData = {
                UserName: reqObj.session.username,
                Name: reqObj.session.name
            }
            Object.assign(sections, loginData);
        }
        renderWithConfig(reqObj, resObj, view, sections);
    });
}

function renderWithConfig(reqObj, resObj, view, data) {
    Config.findOne({}).exec(function (err, config){
        Object.assign(data, {SystemName: config.SystemName});
        resObj.render(view, data);
    });
}

function generateImageName(imageName) {
    return (Date.now().toString() + imageName)
}

